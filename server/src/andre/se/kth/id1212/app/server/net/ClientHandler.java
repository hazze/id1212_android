package andre.se.kth.id1212.app.server.net;

import andre.se.kth.id1212.app.server.common.Message;
import andre.se.kth.id1212.app.server.common.MsgType;
import andre.se.kth.id1212.app.server.controller.Controller;

import java.io.*;
import java.net.Socket;

/**
 * Handles all the different client connections. All messages received from and sent to clients pass through here
 */
class ClientHandler implements Runnable {
    private final GameServer server;
    private final Socket clientSocket;
    private BufferedReader fromClient;
    private PrintWriter toClient;
    private Controller controller;
    private boolean connected;

    /**
     * Creates an instance of the client handler
     * @param server Reference to the server the client connected to.
     * @param clientSocket Reference to the socket the client used to connect.
     * @param controller Reference to the server side controller.
     */
    ClientHandler(GameServer server, Socket clientSocket, Controller controller) {
        this.server = server;
        this.clientSocket = clientSocket;
        this.controller = controller;
        connected = true;
    }

    /**
     * The run loop handling all the communication with the client.
     */
    @Override
    public void run() {
        try {
            toClient = new PrintWriter(clientSocket.getOutputStream(), true);
            fromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
        while (connected) {
            try {
                sendMsg(generateMessageString(controller.parseMessage(fromClient.readLine())));
            }
            catch (IOException e) {
                disconnectClient();
                System.out.println("Client disconnected.");
            }
        }
    }


    private String generateMessageString(Message msg) {
        if (msg.getGameState() != null)
            return msg.getType() + "-" + msg.getGameState().toString();
        return msg.getType() + "-";
    }

    private void sendMsg(String msg) {
        toClient.println(msg);
    }

    private void disconnectClient() {
        try {
            clientSocket.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        connected = false;
        server.removeHandler(this);
    }
}
