package andre.se.kth.id1212.app.server.common;
/**
 * Enum for the different typ of game states.
 */
public enum State {
    INPROGRESS,
    VICTORY,
    GAMEOVER
}
