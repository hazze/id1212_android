package andre.se.kth.id1212.app.server.model;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Holds all the randomized words
 */
public class Word {
    private String s;
    private ArrayList<String> wordList = new ArrayList<>();
    private File file = new File("/Users/hazze/java/net_prog/homework5/server/src/andre/se/kth/id1212/app/server/extras/words.txt");

    public Word() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while ((s = reader.readLine()) != null) wordList.add(s.toLowerCase());
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves a random word from the list, if that word is one character long
     * it's a character dividers in the list describing what character will follow.
     * If this is the case we random again.
     * @return A random word from the list.
     */
    public String getRandomWord() {
        String word = "a";
        while (word.length() < 2)
            word = wordList.get(ThreadLocalRandom.current().nextInt(0, wordList.size()));
        return word;
    }
}
