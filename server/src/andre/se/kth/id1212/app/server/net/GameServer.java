package andre.se.kth.id1212.app.server.net;

import andre.se.kth.id1212.app.server.controller.Controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 * Receives client connections and creates a handler for each client.
 */
public class GameServer {
    private static final int LINGER_TIME = 5000;
    private static final int TIMEOUT_HALF_HOUR = 1800000;
    private final Controller controller = new Controller();
    private final List<ClientHandler> clients = new ArrayList<>();
    private int portNo = 4444;

    /**
     * @param args Takes one command line argument (NOT USED)
     */
    public static void main(String[] args) {
        GameServer server = new GameServer();
        server.serve();
    }

    /**
     * Removes the handler from the active ones because the client has disconnected
     * and the handler is therefor not in use.
     * @param handler Reference to the handler.
     */
    void removeHandler(ClientHandler handler) {
        synchronized (clients) {
            clients.remove(handler);
        }
    }

    private void serve() {
        try {
            ServerSocket listeningSocket = new ServerSocket(portNo);
            System.out.println("Waiting for connections");
            while (true) {
                Socket clientSocket = listeningSocket.accept();
                startHandler(clientSocket);
            }
        } catch (IOException e) {
            System.err.println("Server failure.");
        }
    }

    private void startHandler(Socket clientSocket) throws SocketException {
        clientSocket.setSoLinger(true, LINGER_TIME);
        clientSocket.setSoTimeout(TIMEOUT_HALF_HOUR);
        ClientHandler handler = new ClientHandler(this, clientSocket, controller);
        synchronized (clients) {
            clients.add(handler);
            System.out.println("New connection!");
        }
        Thread handlerThread = new Thread(handler);
        handlerThread.setPriority(Thread.MAX_PRIORITY);
        handlerThread.start();
    }
}
