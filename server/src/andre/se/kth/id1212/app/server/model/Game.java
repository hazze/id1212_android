package andre.se.kth.id1212.app.server.model;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Holds all information for each game, including the correct word, the correctly and wrongly guessed chars,
 * the number of attempts left and the current score.
 */
public class Game implements Serializable {
    private String word;
    private StringBuilder correctLetters;
    private ArrayList<Character> guessedLetters;
    private int attemptsLeft;
    private int currentScore;


    public Game(int score, String randomWord) {
        word = randomWord;
        correctLetters = new StringBuilder();
        for(int i = 0; i < word.length(); i++) {
            correctLetters.append('_');
        }
        guessedLetters = new ArrayList<>();
        attemptsLeft = word.length();
        currentScore = score;
    }

    /**
     *  Adds the guessed character to the array of wrongly guessed chars.
     * @param c The character to add to guessedLetters.
     */
    public void setGuessedLetters(Character c) {
        this.guessedLetters.add(c);
    }

    /**
     * Adds the guessed character to the array of correctly guessed chars.
     * @param c The character to add to guessedLetters.
     * @param index The index for where to add the character
     */
    public void setCorrectLetters(Character c, int index) {
        this.correctLetters.setCharAt(index, c);
    }

    /**
     * Adds correctly guessed chars to the array if the user correctly guessed the word.
     * @param s The string containing the word guessed.
     */
    public void setCorrectLetters(String s) {
        this.correctLetters.replace(0, s.length(), s);
    }

    /**
     * Set the number of attempts left.
     * @param attemptsLeft The integer that hold the number to be set.
     */
    public void setAttemptsLeft(int attemptsLeft) {
        this.attemptsLeft = attemptsLeft;
    }

    /**
     * Increments the current score by one.
     */
    public void incrementScore() {
        this.currentScore++;
    }

    /**
     * Decreases the current score by one.
     */
    public void decreaseScore() {
        this.currentScore--;
    }

    /**
     * Decreases the attempts left by one.
     */
    public void decreaseAttemptsLeft() {
        this.attemptsLeft--;
    }

    /**
     * @return The game's current score.
     */
    public int getCurrentScore() {
        return currentScore;
    }

    /**
     * @return The game's current correct word.
     */
    public String getWord() {
        return word;
    }

    /**
     * @return The game's currently correctly guessed characters.
     */
    public StringBuilder getCorrectLetters() {
        return correctLetters;
    }

    /**
     * @return The game's currently wrongly guessed characters
     */
    public String getGuessedLetters() {
        return guessedLetters.toString();
    }

    /**
     * @return The game's current attempts left.
     */
    public int getAttemptsLeft() {
        return attemptsLeft;
    }
}
