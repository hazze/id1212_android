package andre.se.kth.id1212.app.server.controller;

import andre.se.kth.id1212.app.server.common.GameState;
import andre.se.kth.id1212.app.server.common.Message;
import andre.se.kth.id1212.app.server.common.MsgType;
import andre.se.kth.id1212.app.server.common.State;
import andre.se.kth.id1212.app.server.model.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The server side controller. All calls towards the server side model is passed through here.
 */
public class Controller {
    private final Word words;
    private HashMap<Long, Game> gameList = new HashMap<>();


    /**
     * Creates an instance of the controller containing the reference to the list of words.
     */
    public Controller() {
        this.words = new Word();
    }

    private Message splitMessage(String msg) {
        String[] tmp;
        tmp = msg.split("-");
        if (tmp.length == 1)
            return new Message(MsgType.valueOf(tmp[0]), null, null);
        return new Message(MsgType.valueOf(tmp[0]), tmp[1], null);
    }

    /**
     * Parses all incoming messages and handles them accordingly.
     * @param message The message received that will be parsed.
     * @return The message that will be sent back to the sender of the parsed message.
     */
    public Message parseMessage(String message) {
        Message msg = splitMessage(message);
        long currentThread = Thread.currentThread().getId();
        Game currentGame;
        switch (msg.getType()) {
            case START:
                currentGame = createNewGame(gameList.get(currentThread) == null ? 0 : gameList.get(currentThread).getCurrentScore());
                gameList.put(currentThread, currentGame);
                System.out.println(Thread.currentThread() + " started new game.");
                return new Message(MsgType.GAME, null, generateGameState(currentGame, State.INPROGRESS));
            case LETTER:
                if ((currentGame = getCurrentGame(currentThread)) != null) {
                    if (currentGame.getAttemptsLeft() == 0 && !(currentGame.getWord().equals(currentGame.getCorrectLetters().toString()))) {
                        currentGame.decreaseScore();
                        return new Message(MsgType.GAME, null, generateGameState(currentGame, State.GAMEOVER));
                    }
                    return new Message(MsgType.GAME, null, checkCharacterGuess(currentGame, msg.getMessage().charAt(0)));
                }
                break;
            case WORD:
                if ((currentGame = getCurrentGame(currentThread)) != null) {
                    if (currentGame.getAttemptsLeft() == 0 && !(currentGame.getWord().equals(currentGame.getCorrectLetters().toString()))) {
                        currentGame.decreaseScore();
                        return new Message(MsgType.GAME, null, generateGameState(currentGame, State.GAMEOVER));
                    }
                    return new Message(MsgType.GAME, null, checkWordGuess(currentGame, msg.getMessage()));
                }
                break;
            case DISCONNECT:
                System.out.println("Client disconnected.");
                return new Message(MsgType.DISCONNECT, null, null);
        }
        return new Message(MsgType.DISCONNECT, null, null);
    }

    private Game createNewGame(int score) {
        return new Game(score, words.getRandomWord());
    }

    private GameState generateGameState(Game game, State state) {
        return new GameState(game.getCorrectLetters(), game.getGuessedLetters(), game.getAttemptsLeft(), game.getCurrentScore(), state);
    }

    private Game getCurrentGame(long threadId) {
        return gameList.get(threadId);
    }

    private GameState checkCharacterGuess(Game game, char c) {
        if (game.getAttemptsLeft() > 0 && !(game.getWord().equals(game.getCorrectLetters().toString()))) {
            if (game.getWord().indexOf(c) != -1) {
                int i = game.getWord().indexOf(c);
                while (i >= 0) {
                    game.setCorrectLetters(c, i);
                    i = game.getWord().indexOf(c, i + 1);
                }
            } else {
                if (game.getGuessedLetters().indexOf(c) == -1) {
                    game.setGuessedLetters(c);
                    game.decreaseAttemptsLeft();
                    if (game.getAttemptsLeft() == 0) {
                        game.setCorrectLetters(game.getWord());
                        return generateGameState(game, State.GAMEOVER);
                    }
                }
            }
            if (game.getWord().equals(game.getCorrectLetters().toString())) {
                game.incrementScore();
                game.setAttemptsLeft(0);
                return generateGameState(game, State.VICTORY);
            }
            else {
                return generateGameState(game, State.INPROGRESS);
            }
        }
        game.setCorrectLetters(game.getWord());
        return generateGameState(game, State.GAMEOVER);
    }

    private GameState checkWordGuess(Game game, String word) {
        if (game.getAttemptsLeft() > 0) {
            if (word.equals(game.getWord())) {
                game.setCorrectLetters(word);
                game.incrementScore();
                game.setAttemptsLeft(0);
                return generateGameState(game, State.VICTORY);
            } else {
                game.decreaseAttemptsLeft();
                if (game.getAttemptsLeft() == 0) {
                    game.setCorrectLetters(game.getWord());
                    return generateGameState(game, State.GAMEOVER);
                }
                return generateGameState(game, State.INPROGRESS);
            }
        }
        game.setCorrectLetters(game.getWord());
        return generateGameState(game, State.GAMEOVER);
    }
}

