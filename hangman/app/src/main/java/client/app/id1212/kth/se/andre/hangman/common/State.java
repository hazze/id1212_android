package client.app.id1212.kth.se.andre.hangman.common;
/**
 * Enum for the different typ of game states.
 */
public enum State {
    INPROGRESS,
    VICTORY,
    GAMEOVER
}
