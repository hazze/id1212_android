package client.app.id1212.kth.se.andre.hangman.common;

import java.io.Serializable;

/**
 * Contains the message sent between the server and clients.
 */
public class Message implements Serializable {
    private final MsgType type;
    private final String message;
    private final GameState gameState;

    /**
     * Creates an instance of a message.
     * @param type Sets the type of message.
     * @param message Sets the message text.
     * @param gameState Sets the game state of the message.
     */
    public Message(MsgType type, String message, GameState gameState) {
        this.type = type;
        this.message = message;
        this.gameState = gameState;
    }

    /**
     * @return Returns the current message's text.
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return Returns the current message's game state.
     */
    public GameState getGameState() {
        return gameState;
    }

    /**
     * @return Returns the current message's type.
     */
    public MsgType getType() {
        return type;
    }

    /**
     * @return The message as a string.
     */
    @Override
    public String toString() {
        return "Message{" +
                "type=" + type +
                ", message='" + message + '\'' +
//                ", gameState=" + gameState.toString() +
                '}';
    }
}
