package client.app.id1212.kth.se.andre.hangman.common;

import java.io.Serializable;

/**
 * Contains the current game state.
 * Used for sending state of the game to the client.
 */
public class GameState implements Serializable {
    private StringBuilder correctLetters;
    private String guessedLetters;
    private int attemptsLeft;
    private int currentScore;
    private State state;

    /**
     * Creates an instance of game state
     * @param correctLetters Reference to the correctly guessed characters.
     * @param guessedLetters Reference to the wrongly guessed characters.
     * @param attemptsLeft Reference to the attempts left.
     * @param currentScore Reference to the current score.
     * @param state The state the game is currently in.
     */
    public GameState(StringBuilder correctLetters, String guessedLetters, int attemptsLeft, int currentScore, State state) {
        this.correctLetters = correctLetters;
        this.guessedLetters = guessedLetters;
        this.attemptsLeft = attemptsLeft;
        this.currentScore = currentScore;
        this.state = state;
    }

    /**
     * @return Returns the correctly guessed characters.
     */
    public StringBuilder getCorrectLetters() {
        return correctLetters;
    }

    /**
     * @return Returns the wrongly guessed characters.
     */
    public String getGuessedLetters() {
        return guessedLetters;
    }

    /**
     * @return Returns the attempts left.
     */
    public int getAttemptsLeft() {
        return attemptsLeft;
    }

    /**
     * @return Returns the score.
     */
    public int getCurrentScore() {
        return currentScore;
    }

    /**
     * @return Returns the game state.
     */
    public State getState() {
        return state;
    }

    /**
     * @return Returns the game state as string.
     */
    @Override
    public String toString() {
        return "GameState{" +
                "correctLetters=" + correctLetters +
                ", guessedLetters='" + guessedLetters + '\'' +
                ", attemptsLeft=" + attemptsLeft +
                ", currentScore=" + currentScore +
                ", state=" + state +
                '}';
    }
}
