package client.app.id1212.kth.se.andre.hangman;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.CompletableFuture;

import client.app.id1212.kth.se.andre.hangman.common.Message;
import client.app.id1212.kth.se.andre.hangman.common.MsgType;
import client.app.id1212.kth.se.andre.hangman.net.MessageBus;
import client.app.id1212.kth.se.andre.hangman.net.ServerConnection;

public class Hangman extends AppCompatActivity {
    private final ServerConnection server = new ServerConnection();
    private Button guessButton;
    private TextView score;
    private TextView attempts;
    private TextView word;
    private TextView guessed;
    private TextView gameState;
    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connect("10.0.2.2", 4444, new EventHandler());
        Button newGameButton = findViewById(R.id.buttonNewGame);
        guessButton = findViewById(R.id.buttonGuess);
        Button disconnectButton = findViewById(R.id.buttonDisconnect);

        attempts = findViewById(R.id.numberAttempts);
        score = findViewById(R.id.numberScore);
        word = findViewById(R.id.textWord);
        guessed = findViewById(R.id.textGuessedChars);
        gameState = findViewById(R.id.textGameState);
        input = findViewById(R.id.inputGuess);

        newGameButton.setOnClickListener(view -> {
            Snackbar.make(view, "New Game started!", Snackbar.LENGTH_LONG).show();
            sendMsg(MsgType.START, null);
        });

        guessButton.setOnClickListener(view -> {
            Snackbar.make(view, "Guess sent!", Snackbar.LENGTH_LONG).show();

            if (input.getText().length() > 1) {
                String text = input.getText().toString();
                sendMsg(MsgType.WORD, text.toLowerCase());
            } else {
                String text = input.getText().toString();
                sendMsg(MsgType.LETTER, text.toLowerCase());
            }

            input.setText("");
        });

        disconnectButton.setOnClickListener(view -> {
            Snackbar.make(view, "Disconnected!", Snackbar.LENGTH_LONG).show();
            sendMsg(MsgType.DISCONNECT, null);

            CompletableFuture.runAsync(() -> {
                try {
                    disconnect();
                } catch (IOException ioe) {
                    throw new UncheckedIOException(ioe);
                }
            });
        });

        score.setText("0");
        attempts.setText("0");
        gameState.setText("");
        guessButton.setEnabled(false);
    }

    /**
     * Calls the serverConnection.connect to set up new connection.
     * @param host The ip to use to connect to the host.
     * @param port The port to use to connect to the host.
     * @param bus Called whenever a message is received from the server and needs to be printed.
     */
    private void connect(String host, int port, MessageBus bus) {
        CompletableFuture.runAsync(() -> {
            try {
                server.connect(host, port, bus);
            } catch (IOException ioe) {
                throw new UncheckedIOException(ioe);
            }
        });
    }

    private void disconnect() throws IOException {
        server.disconnect();
    }

    private void sendMsg(MsgType type, String msg) {
        CompletableFuture.runAsync(() -> {
            try {
                switch (type) {
                    case START:
                        server.requestNewGame();
                        break;
                    case LETTER:
                        server.sendCharacterGuess(msg);
                        break;
                    case WORD:
                        server.sendWordGuess(msg);
                        break;
                    case DISCONNECT:
                        server.disconnect();
                        break;
                }
            } catch (IOException ioe) {
                throw new UncheckedIOException(ioe);
            }
        });
    }

    class EventHandler implements MessageBus {
        @Override
        public void handleMsg(Message msg) {
            System.out.println(msg.toString() + msg.getGameState().toString());
            switch (msg.getType()) {
                case GAME:
                    StringBuilder correct = new StringBuilder(msg.getGameState().getCorrectLetters());
                    for (int i = 0; i < correct.length(); i++)
                        if (i % 2 == 1) correct.insert(i, ' ');
                    word.post(() -> word.setText(correct));
                    guessed.post(() -> guessed.setText(msg.getGameState().getGuessedLetters()));
                    attempts.post(() -> attempts.setText(String.valueOf(msg.getGameState().getAttemptsLeft())));
                    score.post(() -> score.setText(String.valueOf(msg.getGameState().getCurrentScore())));
                    switch (msg.getGameState().getState()) {
                        case GAMEOVER:
                            gameState.post(() -> gameState.setText(getResources().getString(R.string.game_over)));
                            guessButton.post(() -> guessButton.setEnabled(false));
                            break;
                        case VICTORY:
                            gameState.post(() -> gameState.setText(getResources().getString(R.string.victory)));
                            guessButton.post(() -> guessButton.setEnabled(false));
                            break;
                        case INPROGRESS:
                            gameState.post(() -> gameState.setText(getResources().getString(R.string.in_progress)));
                            guessButton.post(() -> guessButton.setEnabled(true));
                            break;
                    }
                    break;
                case DISCONNECT:
                    CompletableFuture.runAsync(() -> {
                        try {
                            disconnect();
                        } catch (IOException ioe) {
                            throw new UncheckedIOException(ioe);
                        }
                    });
                    break;
            }
        }
    }
}
