package client.app.id1212.kth.se.andre.hangman.net;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import client.app.id1212.kth.se.andre.hangman.common.GameState;
import client.app.id1212.kth.se.andre.hangman.common.Message;
import client.app.id1212.kth.se.andre.hangman.common.MsgType;
import client.app.id1212.kth.se.andre.hangman.common.State;

/**
 * Manages all communicating with the server.
 */
public class ServerConnection {
    private static final int TIMEOUT_HALF_HOUR = 1800000;
    private static final int TIMEOUT_HALF_MINUTE = 30000;
    private Socket socket;
    private boolean connected;
    private PrintWriter toServer;
    private BufferedReader fromServer;

    /**
     * Sets up the connection to the server
     * @param host The ip for connecting to the host.
     * @param port The port to use when connecting
     * @param bus Reference to the class that handles the incoming server messages.
     * @throws IOException If the connection fails.
     */
    public void connect(String host, int port, MessageBus bus) throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(host, port), TIMEOUT_HALF_MINUTE);
        socket.setSoTimeout(TIMEOUT_HALF_HOUR);
        connected = true;
        toServer = new PrintWriter(socket.getOutputStream(), true);
        fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        new Thread(new Listener(bus)).start();
    }

    /**
     * Sends disconnect messages to the server and closes the connection.
     * @throws IOException If sending the message fails.
     */
    public void disconnect() throws IOException {
        sendMsg(MsgType.DISCONNECT, null);
        socket.close();
        socket = null;
        connected = false;
    }

    /**
     * Requests new game to start.
     */
    public void requestNewGame() {
        sendMsg(MsgType.START, null);
    }

    /**
     * Sends a word guess for a word.
     * @param msg The word to guess.
     */
    public void sendWordGuess(String msg) {
        sendMsg(MsgType.WORD, msg);
    }

    /**
     * Sends a character guess for a word.
     * @param msg The character to guess
     */
    public void sendCharacterGuess(String msg) {
        sendMsg(MsgType.LETTER, msg);
    }

    private void sendMsg(MsgType type, String body) {
        Message msg = new Message(type, body, null);
        toServer.println(createMessageToServer(msg));
    }

    private String createMessageToServer(Message msg) {
        if (msg.getMessage() == null)
            return msg.getType() + "";
        return msg.getType() + "-" + msg.getMessage();
    }

    /**
     * Listener for incoming server messages.
     */
    private class Listener implements Runnable {
        private final MessageBus bus;

        private Listener(MessageBus bus) {
            this.bus = bus;
        }

        /**
         * Runnable function that loops and listens for messages.
         */
        @Override
        public void run() {
            try {
                for (;;) {
                    bus.handleMsg(handleIncMsg(fromServer.readLine()));
                }
            } catch (Exception connectionFailure) {
                if (connected) {
                    connectionFailure.printStackTrace();
                    bus.handleMsg(new Message(MsgType.DISCONNECT, null, null));
                }
            }
        }

        private Message handleIncMsg(String msg) {
            String[] tmp;
            tmp = msg.split("-");
            if (tmp.length == 1)
                return new Message(MsgType.valueOf(tmp[0]), null, null);

            GameState state = new GameState(new StringBuilder(tmp[1]), tmp[2], Integer.parseInt(tmp[3]), Integer.parseInt(tmp[4]), State.valueOf(tmp[5]));
            return new Message(MsgType.valueOf(tmp[0]), null, state);
        }

    }
}
