package client.app.id1212.kth.se.andre.hangman.common;

/**
 * Enum for the different typ of messages
 */
public enum MsgType {
    START,
    DISCONNECT,
    LETTER,
    WORD,
    GAME,
}
