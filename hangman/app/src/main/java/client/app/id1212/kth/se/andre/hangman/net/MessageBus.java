package client.app.id1212.kth.se.andre.hangman.net;

import java.util.ArrayList;

import client.app.id1212.kth.se.andre.hangman.common.Message;

/**
 * Handles messages from the server
 */
public interface MessageBus {
    /**
     * Handles messages from the server.
     * @param msg Message to print.
     */
    void handleMsg(Message msg);
}
